
//-------------- NAV BAR (underline animation)-----------------
class NavLines{
    constructor(){
        
        this.about_us = document.querySelector(".about-us");
        this.home = document.querySelector(".home");
        this.our_projects = document.querySelector(".our-projects");
        this.developers = document.querySelector(".developers");
        this.contact = document.querySelector(".contact");

        this.nav_ind = document.querySelector(".navbar__indicator");
        
        this.events();   
    }
     events(){
         this.home.onclick = function(){
            this.nav_ind.style.transition = "all 0.3s ease";
            this.nav_ind.style.left = "460px";
        }
        this.about_us.onclick = function(){
            this.nav_ind.style.transition = "all 0.3s ease";
            this.nav_ind.style.left = "576px";
        }
        this.our_projects.onclick = function(){
           this.nav_ind.style.transition = "all 0.3s ease";
            this.nav_ind.style.left = "722px";
        }
        this.developers.onclick = function(){
            this.nav_ind.style.transition = "all 0.3s ease";
            this.nav_ind.style.left = "867px";
            this.nav_ind.style.transform = "scaleX(100%)";
        }
        this.contact.onclick = function(){
            this.nav_ind.style.transition = "all 0.3s ease";
            this.nav_ind.style.left = "1000px";
        }
     }       
}

export default NavLines;