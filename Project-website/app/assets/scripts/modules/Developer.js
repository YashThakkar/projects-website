
import $ from 'jquery';
import 'owl.carousel';
//import 'owl.carousel/dist/assets/owl.carousel.css';

class Developer{
    constructor(){
//        this.slider = $("#developer-slider");
        $(document).ready(function(){
            $("#developer-slider").owlCarousel({
               items: 2,
               autoplay: true,
               margin: 20,
               loop: true,
               nav: true,
               smartspeed: 700,
               autoplayHoverPause: true,
               dots: false,
               navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>']
           });
        });
    }
}

export default Developer;