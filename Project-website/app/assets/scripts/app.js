import $ from 'jquery';
import "../styles/style.css";
//import NavLines from "./modules/NavLines";
//import Developer from "./modules/Developer";

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
import "lazysizes";
import 'responsive-tabs';
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import WOW from 'wowjs';
import animate from "animate.css";

//Hot Injection for js 
if(module.hot){
    module.hot.accept();
}

// WOW initialization
new WOW.WOW().init()

/************************************************************************
                        PRELOADER
************************************************************************/
$(window).on('load', function(){
    $("#preloader").delay(3500).fadeOut("slow");
});


let mobileMenu = new MobileMenu();

new RevealOnScroll($(".section"), "40%");
let smoothScroll = new SmoothScroll();

$(document).ready(function(){
   
  $('#developer-slider').owlCarousel({
        items:3,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
      
      responsive: {
           0:{
               items: 1,
               nav: true
           },
            480:{
               items: 1,
                nav: false
           },
            768:{
               items: 3
           }
       }
  });
    $('#frontend-slider').owlCarousel({
        items:1,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        dots: false,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>']
  });
    $('#backend-slider').owlCarousel({
        items:1,
        loop:true,
        margin:0,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        dots: false,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>']
  });

});


window.onload = function(){
    // Responsive tabs
//     $('#projects-tabs').responsiveTabs({
//        animation: 'slide'
//    });
      
    //-------------- NAV BAR (underline animation)-----------------
    var about_us = document.querySelector(".about-us-line");
    var home = document.querySelector(".home-line");
    var our_projects = document.querySelector(".our-projects-line");
    var developers = document.querySelector(".developers-line");
    var contact = document.querySelector(".contact-line");
    
    var nav_ind = document.querySelector(".navbar__indicator");

    home.onclick = function(){
        nav_ind.style.transition = "all 0.3s ease";
//        nav_ind.style.left = "454px";
        nav_ind.style.left = "31%";
    }
    about_us.onclick = function(){
        nav_ind.style.transition = "all 0.3s ease";
//        nav_ind.style.left = "568px";
        nav_ind.style.left = "38%";
    }
    our_projects.onclick = function(){
        nav_ind.style.transition = "all 0.3s ease";
//        nav_ind.style.left = "714px";
        nav_ind.style.left = "47%";
    }
    developers.onclick = function(){
        nav_ind.style.transition = "all 0.3s ease";
//        nav_ind.style.left = "867px";
        nav_ind.style.left = "56%";
//        nav_ind.style.transform = "scaleX(100%)";
    }
    contact.onclick = function(){
        nav_ind.style.transition = "all 0.3s ease";
//        nav_ind.style.left = "1000px";
        nav_ind.style.left = "64.4%";
    }
    
    //---------------Tabs--------------
    
    var left_tab = document.querySelector(".home-page__right__tabs__left-tab");
    var right_tab = document.querySelector(".home-page__right__tabs__right-tab");
    
    var left_tab_content = document.querySelector(".main-left");
    var right_tab_content = document.querySelector(".main-right");
    
//    if(left_tab.classList.contains("inactive-tab")){
//        left_tab.style.hover(function(){
//           left_tab.style.background = "#C223CE";
//        });
//    }
    
    left_tab.onclick = function(){
        if(right_tab.classList.contains("active-tab")){
            right_tab.classList.remove("active-tab");
            right_tab.classList.add("inactive-tab");
            left_tab.classList.remove("inactive-tab");
            left_tab.classList.add("active-tab");
            
            right_tab_content.classList.remove("active-tab-content");
            right_tab_content.classList.add("inactive-tab-content");
            
            left_tab_content.classList.remove("inactive-tab-content");
            left_tab_content.classList.add("active-tab-content");
        }  
    }
    right_tab.onclick = function(){
        if(left_tab.classList.contains("active-tab")){
            left_tab.classList.remove("active-tab");
            left_tab.classList.add("inactive-tab");
            right_tab.classList.remove("inactive-tab");
            right_tab.classList.add("active-tab");
            
            left_tab_content.classList.remove("active-tab-content");
            left_tab_content.classList.add("inactive-tab-content");
            
            right_tab_content.classList.remove("inactive-tab-content");
            right_tab_content.classList.add("active-tab-content");
        }  
    }
    
    //------for standalone Inner tabs------------
    var c_lang = document.querySelector(".c-lang");
    var cplus_lang = document.querySelector(".cplus-lang");
    var java_lang = document.querySelector(".java-lang");
    var python_lang = document.querySelector(".python-lang");
    
    var c_lang_content = document.querySelector(".c-lang-content");
    var cplus_lang_content = document.querySelector(".cplus-lang-content");
    var java_lang_content = document.querySelector(".java-lang-content");
    var python_lang_content = document.querySelector(".python-lang-content");
    
    c_lang.onclick = function(){
        if(c_lang.classList.contains("inactive-tab")){
            c_lang.classList.remove("inactive-tab");
            c_lang.classList.add("active-tab");
            
            //activating its content
            c_lang_content.classList.remove("inactive-tab-content");
            c_lang_content.classList.add("active-tab-content");
        }
        if(cplus_lang.classList.contains("active-tab")){
            cplus_lang.classList.remove("active-tab");
            cplus_lang.classList.add("inactive-tab");
            //activating its content
            cplus_lang_content.classList.remove("active-tab-content");
            cplus_lang_content.classList.add("inactive-tab-content");
        }
        if(java_lang.classList.contains("active-tab")){
            java_lang.classList.remove("active-tab");
            java_lang.classList.add("inactive-tab");
            //activating its content
            java_lang_content.classList.remove("active-tab-content");
            java_lang_content.classList.add("inactive-tab-content");
        }
        if(python_lang.classList.contains("active-tab")){
            python_lang.classList.remove("active-tab");
            python_lang.classList.add("inactive-tab");
            //activating its content
            python_lang_content.classList.remove("active-tab-content");
            python_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    cplus_lang.onclick = function(){
        if(cplus_lang.classList.contains("inactive-tab")){
            cplus_lang.classList.remove("inactive-tab");
            cplus_lang.classList.add("active-tab");
            //activating its content
            cplus_lang_content.classList.remove("inactive-tab-content");
            cplus_lang_content.classList.add("active-tab-content");
        }
        if(c_lang.classList.contains("active-tab")){
            c_lang.classList.remove("active-tab");
            c_lang.classList.add("inactive-tab");
            //activating its content
            c_lang_content.classList.remove("active-tab-content");
            c_lang_content.classList.add("inactive-tab-content");
        }
        if(java_lang.classList.contains("active-tab")){
            java_lang.classList.remove("active-tab");
            java_lang.classList.add("inactive-tab");
            //activating its content
            java_lang_content.classList.remove("active-tab-content");
            java_lang_content.classList.add("inactive-tab-content");
        }
        if(python_lang.classList.contains("active-tab")){
            python_lang.classList.remove("active-tab");
            python_lang.classList.add("inactive-tab");
            //activating its content
            python_lang_content.classList.remove("active-tab-content");
            python_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    java_lang.onclick = function(){
        if(java_lang.classList.contains("inactive-tab")){
            java_lang.classList.remove("inactive-tab");
            java_lang.classList.add("active-tab");
            //activating its content
            java_lang_content.classList.remove("inactive-tab-content");
            java_lang_content.classList.add("active-tab-content");
        }
        if(c_lang.classList.contains("active-tab")){
            c_lang.classList.remove("active-tab");
            c_lang.classList.add("inactive-tab");
            //activating its content
            c_lang_content.classList.remove("active-tab-content");
            c_lang_content.classList.add("inactive-tab-content");
        }
        if(cplus_lang.classList.contains("active-tab")){
            cplus_lang.classList.remove("active-tab");
            cplus_lang.classList.add("inactive-tab");
            //activating its content
            cplus_lang_content.classList.remove("active-tab-content");
            cplus_lang_content.classList.add("inactive-tab-content");
        }
        if(python_lang.classList.contains("active-tab")){
            python_lang.classList.remove("active-tab");
            python_lang.classList.add("inactive-tab");
            //activating its content
            python_lang_content.classList.remove("active-tab-content");
            python_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    python_lang.onclick = function(){
        if(python_lang.classList.contains("inactive-tab")){
            python_lang.classList.remove("inactive-tab");
            python_lang.classList.add("active-tab");
            //activating its content
            python_lang_content.classList.remove("inactive-tab-content");
            python_lang_content.classList.add("active-tab-content");
        }
        if(c_lang.classList.contains("active-tab")){
            c_lang.classList.remove("active-tab");
            c_lang.classList.add("inactive-tab");
            //activating its content
            c_lang_content.classList.remove("active-tab-content");
            c_lang_content.classList.add("inactive-tab-content");
        }
        if(cplus_lang.classList.contains("active-tab")){
            cplus_lang.classList.remove("active-tab");
            cplus_lang.classList.add("inactive-tab");
            //activating its content
            cplus_lang_content.classList.remove("active-tab-content");
            cplus_lang_content.classList.add("inactive-tab-content");
        }
        if(java_lang.classList.contains("active-tab")){
            java_lang.classList.remove("active-tab");
            java_lang.classList.add("inactive-tab");
            //activating its content
            java_lang_content.classList.remove("active-tab-content");
            java_lang_content.classList.add("inactive-tab-content");
        }
    }  
    
    //------for Web Inner tabs------------
    var html_lang = document.querySelector(".html-lang");
    var css_lang = document.querySelector(".css-lang");
    var js_lang = document.querySelector(".js-lang");
    var php_lang = document.querySelector(".php-lang");
    
    var html_lang_content = document.querySelector(".html-lang-content");
    var css_lang_content = document.querySelector(".css-lang-content");
    var js_lang_content = document.querySelector(".js-lang-content");
    var php_lang_content = document.querySelector(".php-lang-content");
    
    html_lang.onclick = function(){
        if(html_lang.classList.contains("inactive-tab")){
            html_lang.classList.remove("inactive-tab");
            html_lang.classList.add("active-tab");
            
            //activating its content
            html_lang_content.classList.remove("inactive-tab-content");
            html_lang_content.classList.add("active-tab-content");
        }
        if(css_lang.classList.contains("active-tab")){
            css_lang.classList.remove("active-tab");
            css_lang.classList.add("inactive-tab");
            //activating its content
            css_lang_content.classList.remove("active-tab-content");
            css_lang_content.classList.add("inactive-tab-content");
        }
        if(js_lang.classList.contains("active-tab")){
            js_lang.classList.remove("active-tab");
            js_lang.classList.add("inactive-tab");
            //activating its content
            js_lang_content.classList.remove("active-tab-content");
            js_lang_content.classList.add("inactive-tab-content");
        }
        if(php_lang.classList.contains("active-tab")){
            php_lang.classList.remove("active-tab");
            php_lang.classList.add("inactive-tab");
            //activating its content
            php_lang_content.classList.remove("active-tab-content");
            php_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    css_lang.onclick = function(){
        if(css_lang.classList.contains("inactive-tab")){
            css_lang.classList.remove("inactive-tab");
            css_lang.classList.add("active-tab");
            //activating its content
            css_lang_content.classList.remove("inactive-tab-content");
            css_lang_content.classList.add("active-tab-content");
        }
        if(html_lang.classList.contains("active-tab")){
            html_lang.classList.remove("active-tab");
            html_lang.classList.add("inactive-tab");
            //activating its content
            html_lang_content.classList.remove("active-tab-content");
            html_lang_content.classList.add("inactive-tab-content");
        }
        if(js_lang.classList.contains("active-tab")){
            js_lang.classList.remove("active-tab");
            js_lang.classList.add("inactive-tab");
            //activating its content
            js_lang_content.classList.remove("active-tab-content");
            js_lang_content.classList.add("inactive-tab-content");
        }
        if(php_lang.classList.contains("active-tab")){
            php_lang.classList.remove("active-tab");
            php_lang.classList.add("inactive-tab");
            //activating its content
            php_lang_content.classList.remove("active-tab-content");
            php_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    js_lang.onclick = function(){
        if(js_lang.classList.contains("inactive-tab")){
            js_lang.classList.remove("inactive-tab");
            js_lang.classList.add("active-tab");
            //activating its content
            js_lang_content.classList.remove("inactive-tab-content");
            js_lang_content.classList.add("active-tab-content");
        }
        if(html_lang.classList.contains("active-tab")){
            html_lang.classList.remove("active-tab");
            html_lang.classList.add("inactive-tab");
            //activating its content
            html_lang_content.classList.remove("active-tab-content");
            html_lang_content.classList.add("inactive-tab-content");
        }
        if(css_lang.classList.contains("active-tab")){
            css_lang.classList.remove("active-tab");
            css_lang.classList.add("inactive-tab");
            //activating its content
            css_lang_content.classList.remove("active-tab-content");
            css_lang_content.classList.add("inactive-tab-content");
        }
        if(php_lang.classList.contains("active-tab")){
            php_lang.classList.remove("active-tab");
            php_lang.classList.add("inactive-tab");
            //activating its content
            php_lang_content.classList.remove("active-tab-content");
            php_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    php_lang.onclick = function(){
        if(php_lang.classList.contains("inactive-tab")){
            php_lang.classList.remove("inactive-tab");
            php_lang.classList.add("active-tab");
            //activating its content
            php_lang_content.classList.remove("inactive-tab-content");
            php_lang_content.classList.add("active-tab-content");
        }
        if(html_lang.classList.contains("active-tab")){
            html_lang.classList.remove("active-tab");
            html_lang.classList.add("inactive-tab");
            //activating its content
            html_lang_content.classList.remove("active-tab-content");
            html_lang_content.classList.add("inactive-tab-content");
        }
        if(css_lang.classList.contains("active-tab")){
            css_lang.classList.remove("active-tab");
            css_lang.classList.add("inactive-tab");
            //activating its content
            css_lang_content.classList.remove("active-tab-content");
            css_lang_content.classList.add("inactive-tab-content");
        }
        if(js_lang.classList.contains("active-tab")){
            js_lang.classList.remove("active-tab");
            js_lang.classList.add("inactive-tab");
            //activating its content
            js_lang_content.classList.remove("active-tab-content");
            js_lang_content.classList.add("inactive-tab-content");
        }
    }
    
    //

    $("#developer_one").click(function(){
       $("#developer_one").parent().toggleClass("rotate-style");
        $("#developer_one").parent().children().children().children().toggleClass("display-none");
        $(".content-one").toggleClass("toggle-rotate");
        $("#developer_one").toggleClass("toggle-rotate");
    });
    $("#developer_two").click(function(){
       $("#developer_two").parent().toggleClass("rotate-style");
        $("#developer_two").parent().children().children().children().toggleClass("display-none");
        $(".content-two").toggleClass("toggle-rotate");
        $("#developer_two").toggleClass("toggle-rotate");
    });
    $("#developer_three").click(function(){
    alert("threeee");
       $("#developer_three").parent().toggleClass("rotate-style");
        $("#developer_three").parent().children().children().children().toggleClass("display-none");
        $(".content-three").toggleClass("toggle-rotate");
        $("#developer_three").toggleClass("toggle-rotate");
    });
    $("#developer_four").click(function(){
       $("#developer_four").parent().toggleClass("rotate-style");
        $("#developer_four").parent().children().children().children().toggleClass("display-none");
        $(".content-four").toggleClass("toggle-rotate");
        $("#developer_four").toggleClass("toggle-rotate");
    });
    $("#developer_five").click(function(){
       $("#developer_five").parent().toggleClass("rotate-style");
        $("#developer_five").parent().children().children().children().toggleClass("display-none");
        $(".content-five").toggleClass("toggle-rotate");
        $("#developer_five").toggleClass("toggle-rotate");
    });
    
    
    
    
} 

